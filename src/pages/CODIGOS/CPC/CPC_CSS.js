
import React from 'react';
import { StyleSheet } from 'react-native';

const stylesCF = StyleSheet.create({

    containerRemissao:{
      borderWidth: 0.5,
      padding: 5,
      margin: 2,


    },

    containerGeral:{
      borderWidth: 0.5,
      padding: 5,
      margin: 2,


    },


    remissao: {
      fontSize: 13,
      textAlign: 'justify',
      lineHeight: 14,
      color: 'blue',
    },

    remissaoLocked: {
        fontSize: 11,
        color: '#C0C0C0',
      },

    linha1_entrada: {
        fontSize: 16,
        textAlign: 'center',

    },
 
  });

  export default stylesCF;