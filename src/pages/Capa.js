import React from 'react';
import { TouchableHighlight, StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class Capa extends React.Component {

  // constructor(props){

  //   super(props);


  // }

  render(){

    return(


  <View style={{ flex: 1, flexDirection: 'row' }}>
    <View style={{ flex: 8, borderWidth: 1 }}>
      <View style={styles.coluna1}>
        <View style={styles.line}>
          <Text style={styles.titulo}>VMR</Text>
        </View>
        <View style={styles.line}>
          <Text style={styles.subtitulo1}>Vade Mecum com remissões.</Text>
        </View>
        <View style={styles.line}>
          <Text style={styles.subtitulo2}>Direito Atual</Text>
        </View>
      </View>
    </View>
    <View style={{ flex: 2, borderWidth: 1 }}>
      <View style={styles.coluna2}>
        <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Texto')}
            underlayColor="blue" >
            <View style={styles.tagLine}>
              <Text style={styles.tag}>CF</Text>
            </View>
        </TouchableHighlight>
          <View style={styles.tagLine}>
            <Text style={styles.tag}>CC</Text>
          </View>
          <View style={styles.tagLine}>
            <Text style={styles.tag}>CC</Text>
          </View>
      </View>
    </View>
  </View>

    );
  }
};


const styles = StyleSheet.create({

  coluna1: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: '#f4ce42',
  },

  coluna2: {
    flex: 1,
    flexDirection: 'column',
    //justifyContent: 'flex-start',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'blue',
  },

  line: {
    /*width: 200, height: 50,*/
  },

  column: {
    backgroundColor: '#fff',
    alignItems: 'center',
  },

  tagLine: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    borderWidth: 1,
    borderColor: 'green',
  },

  titulo: {
    fontSize: 120,
    fontWeight: 'bold',
    color: 'green',
  },

  subtitulo1: {
    fontSize: 20,
    color: 'black',
  },

  subtitulo2: {
    fontSize: 40,
    color: 'red',
  },

  tag: {
    fontSize: 40,
    color: 'green',
    fontWeight: 'bold',


  },

});
