import React from 'react';
import { Text, View } from 'react-native';

import stylesCF from '../pages/CODIGOS/CPC/CPC_CSS';


function VMText (props) {

        return(
        <View style={stylesCF.containerGeral}> 
            <Text selectable={true}>
                {props.children}
            </Text>
        </View>
        );
}

export default VMText;