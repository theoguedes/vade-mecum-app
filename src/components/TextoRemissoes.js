import React from 'react';
import { Text, View } from 'react-native';

import stylesCF from '../pages/CODIGOS/CPC/CPC_CSS';

function TextoRemissoes (props) {

    const { remissoesAbertas } = props;

        return(
            
            [ remissoesAbertas ?
                <View key={'1'} style={stylesCF.containerRemissao}> 
                    <Text style={stylesCF.remissao} selectable={true}>
                        {props.children}
                    </Text>
                </View>
            : 
            <View  key={'2'} style={stylesCF.containerRemissao}> 
                <Text style={stylesCF.remissaoLocked} selectable={false}>
                    Assista a um vídeo promocional para liberar o conteúdo completo por 1 hora. ;)
                </Text>
            </View>
            ]
            
        );
}

export default TextoRemissoes;