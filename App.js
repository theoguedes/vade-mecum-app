import React from 'react';
import { Button, TouchableHighlight, StyleSheet, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation';

//Components
//import Capa from './src/pages/Capa';

//Pages/Screens
//CPC
import IndiceCPC from './src/pages/CODIGOS/CPC/IndiceCPC'; 
import ExpoCPC from './src/pages/CODIGOS/CPC/ExpoCPC'; 
import CPC from './src/pages/CODIGOS/CPC/CPC';




const styles = StyleSheet.create({

  coluna1: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: '#f4ce42',
  },

  coluna2: {
    flex: 1,
    flexDirection: 'column',
    //justifyContent: 'flex-start',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'blue',
  },

  line: {
    /*width: 200, height: 50,*/
  },

  column: {
    backgroundColor: '#fff',
    alignItems: 'center',
  },

  tagLine: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    borderWidth: 1,
    borderColor: 'green',
  },

  titulo: {
    fontSize: 80,
    fontWeight: 'bold',
    color: 'green',
  },

  subtitulo1: {
    fontSize: 40,
    color: 'black',
  },

  subtitulo2: {
    fontSize: 25,
    color: 'red',
  },

  tag: {
    fontSize: 20,
    color: 'green',
    fontWeight: 'bold',


  },

});


class HomePage extends React.Component {

  constructor(props){

    super(props)

  }

  static navigationOptions = { header: null };

  render(){

    return(

      <View style={{ flex: 1, flexDirection: 'row' }}>
      <View style={{ flex: 8, borderWidth: 1 }}>
        <View style={styles.coluna1}>
          <View style={styles.line}>
            <Text style={styles.titulo}>CODLEX</Text>
          </View>
          <View style={styles.line}>
            <Text style={styles.subtitulo1}>CÓDIGOS</Text>
          </View>
          <View style={styles.line}>
            <Text style={styles.subtitulo2}>Seus códigos com remissões</Text>
          </View>
        </View>
      </View>
      <View style={{ flex: 2, borderWidth: 1 }}>
        <View style={styles.coluna2}>
          <TouchableHighlight
              onPress={() => this.props.navigation.navigate('IndiceCPC')}>
              <View style={styles.tagLine}>
                <Text style={styles.tag}>Índice do CPC</Text>
              </View>
          </TouchableHighlight>
        </View>
          <View style={styles.coluna2}>
          <TouchableHighlight
              onPress={() => this.props.navigation.navigate('ExpoCPC')}
              >
              <View style={styles.tagLine}>
                <Text style={styles.tag}>Exposição de Motivos do CPC</Text>
              </View>
          </TouchableHighlight>
          </View>
          <View style={styles.coluna2}>
          <TouchableHighlight
              onPress={() => this.props.navigation.navigate('CPC')}>
              <View style={styles.tagLine}>
                <Text style={styles.tag}>CPC</Text>
              </View>
          </TouchableHighlight>
          </View>
        </View>
      </View>

    );

  }

}

const RootStack = createStackNavigator({
  Home: HomePage,
  IndiceCPC: IndiceCPC,
  ExpoCPC: ExpoCPC,
  CPC: CPC,
},
  {
    initialRouteName: 'Home',
    headerMode: 'float',
    headerTitleStyle: {
        color: 'white',
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
        marginBottom: 20,
        marginTop: 10,
    }
  },
);

export default class App extends React.Component {

  render() {

    return <RootStack />

  }

} 